# openpandora-devuan

Devuan for OpenPandora, using the arch "armhf"


# Content



1.) The mbr with partion size: https://gitlab.com/openbsd98324/openpandora-devuan/-/raw/main/v2/ascii-armhf/linux-fs-pandora/image-mbr-mmc-pandora.img.gz
    
    Partition 1, Fat/Fat32

2.) Copy the /boot from the following into the root of mmcblk0p1 (autoboot and uImage file that are corresponding).
    

3.) Base rootfs for 2. partition:    https://gitlab.com/openbsd98324/openpandora-devuan/-/raw/main/v2/ascii-armhf/openpandora-devuan-ascii-rootfs-base-v1.tar.gz

    Partition 2., Ext3


# Wifi 

Manual method with interfaces:


  wpa_passphrase MYROUTER SECRETKEY > /etc/wpa_supplicant.conf 
  
  sh /etc/getnet.sh  
  


![](image/openpandora-running-devuan-linux-ascii-v1.jpg)


